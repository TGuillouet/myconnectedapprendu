import Home from "./Home";
import Cam from "./Cam";
import Bot from "./Bot";
import Tabs from "./Tabs";

export {
  Home,
  Cam,
  Bot,
  Tabs
};

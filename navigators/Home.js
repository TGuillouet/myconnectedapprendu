import React, { Component } from "react";
import { createStackNavigator } from "react-navigation-stack";
import { HomeScreen } from "./../components/HomeScreen";
import { TicketsList } from "./../components/TicketsList";

const HomeStack = createStackNavigator({
  HomeScreen: {
    screen: HomeScreen
  },
  TicketList: {
    screen: TicketsList
  }
});

export default HomeStack;
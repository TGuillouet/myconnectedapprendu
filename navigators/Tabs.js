import React, { Component } from "react";
import { createBottomTabNavigator } from "react-navigation-tabs";
import { TabBar } from "./../components/TabBar";
import { Home, Bot, Cam } from "./";
import { DamovoScreen } from "../components/DamovoScreen";

const TabNavigator = createBottomTabNavigator(
  {
    HomeScreenNav: {
      screen: Home
    },
    BotNav: {
      screen: Bot
    },
    CameraNav: {
      screen: Cam
    },
    Damovo: {
      screen: DamovoScreen
    }
  },
  {
    tabBarComponent: TabBar,
    tabBarOptions: {
      activeTintColor: '',
      inactiveTintColor: ''
    }
  }
);

export default TabNavigator;

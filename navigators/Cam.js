import React, { Component } from "react";
import { createStackNavigator } from "react-navigation-stack";
import Camera from "./../components/Camera";
import Ticket from "./../components/Ticket";

const CamStack = createStackNavigator({
  Ticket: {
    screen: Ticket
  },
  CameraModule: {
    screen: Camera
  }
});

export default CamStack;
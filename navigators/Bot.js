import React, { Component } from "react";
import { createStackNavigator } from "react-navigation-stack";
import Bot from "./../components/Bot";

const BotStack = createStackNavigator({
  Bot: {
    screen: Bot
  }
});

export default BotStack;
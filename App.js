import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { createStore } from "redux";
import { Provider } from "react-redux";
import {
  createAppContainer,
  createSwitchNavigator
} from "react-navigation";

import { Tabs } from "./navigators";

import Login from "./components/Login";
import reducer from "./reducer";

const store = createStore(reducer);

const AppNavigator = createSwitchNavigator(
  {

    Login: {
      screen: Login,
    },
    HomeScreenStack: {
      screen: Tabs,
    }
  },
  {
    initialRouteName: "Login"
  }
);

const AppContainer = createAppContainer(AppNavigator);

/**
 * Class for application.
 *
 * @class      App (name)
 */
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <AppContainer />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});

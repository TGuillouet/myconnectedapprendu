export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_STATUS = "UPDATE_STATUS";
export const UPDATE_AUTH = "UPDATE_AUTH";
export const CREATE_TICKET = "CREATE_TICKET";
export const UPDATE_TICKET = "UPDATE_TICKET";
export const UPDATE_OFFICE = "UPDATE_OFFICE";

const initialState = { userInfo: {}, userStatus: "noStatus", serverAuth: "", ticketInfo: {}, officeInfo: {} };

/**
 * { function_description }
 *
 * @param      {<type>}  state   The state
 * @param      {<type>}  action  The action
 * @return     {Object}  { state }
 */
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "UPDATE_USER":
      return { ...state, userInfo: action.payload };
    case "UPDATE_STATUS":
      return { ...state, userStatus: action.payload };
    case "UPDATE_AUTH":
      return { ...state, serverAuth: action.payload };
    case "CREATE_TICKET":
      return { ...state, ticketInfo: action.payload };
    case "UPDATE_TICKET":
      return { ...state, ticketInfo: action.payload };
    case "UPDATE_OFFICE":
      return { ...state, officeInfo: action.payload };
    default:
      return state;
  }
}

/**
 * { function_description }
 *
 * @param      {<type>}  userInfo  The user information
 * @return     {Object}  { description_of_the_return_value }
 */
export function updateUser(userInfo) {
  return {
    type: UPDATE_USER,
    payload: userInfo
  };
}

/**
 * { function_description }
 *
 * @param      {<type>}  userStatus  The user status
 * @return     {Object}  { description_of_the_return_value }
 */
export function updateStatus(userStatus) {
  return {
    type: UPDATE_STATUS,
    payload: userStatus
  };
}

/**
 * { function_description }
 *
 * @param      {<type>}  serverAuth  The server auth
 * @return     {Object}  { description_of_the_return_value }
 */
export function updateAuth(serverAuth) {
  return {
    type: UPDATE_AUTH,
    payload: serverAuth
  };
}

/**
 * Creates a picker ticket.
 *
 * @class      CreatePickerTicket (name)
 * @param      {<type>}  ticketInfo  The ticket information
 * @return     {Object}  { description_of_the_return_value }
 */
export function CreatePickerTicket(ticketInfo) {
  return {
    type: CREATE_TICKET,
    payload: ticketInfo
  };
}

/**
 * { function_description }
 *
 * @param      {<type>}  ticketInfo  The ticket information
 * @return     {Object}  { description_of_the_return_value }
 */
export function updatePickerTicket(ticketInfo) {
  return {
    type: UPDATE_TICKET,
    payload: ticketInfo
  };
}

/**
 * { function_description }
 *
 * @param      {<type>}  officeInfo  The ticket information
 * @return     {Object}  { description_of_the_return_value }
 */
export function updateOfficeInfo(officeInfo) {
  return {
    type: UPDATE_OFFICE,
    payload: officeInfo
  };
}

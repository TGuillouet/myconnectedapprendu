import React, {
  Component
} from "react";
import axios from "axios";
import Geolocation from 'react-native-geolocation-service';
import { ImageBackground, Platform, Linking, TouchableOpacity, Alert, PermissionsAndroid, ToastAndroid } from "react-native";
import { connect } from "react-redux";
import { GoogleSignin } from "react-native-google-signin";
import { updateStatus, updateOfficeInfo } from "../../reducer";
import IncidentsList from "../IncidentsList/IncidentsList";
import {
  Container,
  Button,
  Text,
  Icon
} from "native-base";
import firebase from "../firebaseConfig";
import config from './../../assets/config.json';
import LoadingPopup from "../LoadingPopup";
import styles from "./styles";

/**
 * Class for home screen.
 *
 * @class      HomeScreen (name)
 */
class HomeScreen extends Component {
  localCoord = [0, 0];
  officeCoord = [0, 0];
  officeName = "";

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Accueil",
      headerLeft: (
        <Button transparent onPress={() => this.deconnection(navigation)}>
          <Icon name="power" />
        </Button>
      )
    };
  };

  /**
   * { function_description }
   *
   * @param      {<type>}  navigation  The navigation
   */
  static deconnection(navigation) {
    GoogleSignin.signOut();
    navigation.navigate("Login");
  }

  state = {
    nbrEmail: 0,
    waitStat: "#E51616", // Par défaut rouge
    tickets: []
  };

  componentDidMount() {
    // GPS
    this.gpsLocal();

    // Utils
    this.getTicketNbr();

    // Update status
    const statut = this.props.navigation.getParam("statut", "error");
    this.props.updateS(statut);

    // Reload the tickets number when we load this screen
    this.willFocus = this.props.navigation.addListener('willFocus', (route) => {
      if (route.state.routeName === "HomeScreen") this.getTicketNbr();
    });
  }

  componentWillUnmount() {
    this.willFocus.remove();
  }

  /**
   * Check the permission for the geolocation
   * If the permission is not granted, then request it, else we get the position
   */
  gpsLocal() {
    PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    )
      .then(granted => {
        if (granted) this.checkPos();
        else this.request();
      })
      .catch(e => {
        Alert.alert(
          'Erreur',
          "Une erreur s'est produite lors de la vérification des permissions",
        );
      });
  }

  // Get the position
  checkPos = () => {
    this.setState({ isLoading: true }, () => {
      Geolocation.getCurrentPosition(
        position => {
          this.setState({ isLoading: false }, async () => {
            this.localCoord = [
              position.coords.latitude,
              position.coords.longitude
            ];
            await this.gpsLocationChecker();
          });
        },
        error => {
          // See error code charts below.
          this.setState({ isLoading: false }, () => {
            Alert.alert(
              'Erreur',
              "Une erreur s'est produite pendant la récupération de votre position",
            );
          })
        },
        {
          enableHighAccuracy: true,
          timeout: config.globalTimeout,
          maximumAge: 15000,
        },
      );
    });
  };

  // Make the permission request to use the geolocation
  request = () => {
    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    )
      .then(granted => {
        if (granted === PermissionsAndroid.RESULTS.GRANTED) this.checkPos();
        else this.request();
      })
      .catch(e => {
        Alert.alert(
          'Erreur',
          "Une erreur s'est produite lors de l'attribution des permissions",
        );
      });
  };

  deg2rad(x) {
    return Math.PI * x / 180;
  }

  distance = (lat1, lng1, lat2, lng2) => {
    const earth_radius = 6378000;   // Terre = sphère de 6378km de rayon
    const rlo1 = this.deg2rad(lng1);    // CONVERSION
    const rla1 = this.deg2rad(lat1);
    const rlo2 = this.deg2rad(lng2);
    const rla2 = this.deg2rad(lat2);
    const dlo = (rlo2 - rlo1) / 2;
    const dla = (rla2 - rla1) / 2;
    const a = (Math.sin(dla) * Math.sin(dla)) + Math.cos(rla1) * Math.cos(rla2) * (Math.sin(dlo) * Math.sin(dlo));
    const d = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return (earth_radius * d);
  }

  getOffices = async (userEmail) => {
    try {
      const userReq = await firebase.database().ref("/Users/").orderByChild("email").equalTo(userEmail).once("value");
      const user = Object.values(userReq.val())[0];
      const officesRequest = await firebase
        .database()
        .ref(`/Offices/`)
        .once("value")
      const offices = officesRequest.val();
      const keys = Object.keys(offices);
      return keys.map(key => ({ officeName: key, ...offices[key] }));
    } catch(e) {
      console.log(e);
      ToastAndroid.show("Une erreur s'est produite lors de la récupération des bureaux")
      return [];
    }
  }

  /**
   * { function_description }
   */
  gpsLocationChecker = async () => {
    const offices = await this.getOffices(this.props.userInfo.email);

    offices.forEach(office => {
      this.officeCoord = [
        office.gpsCoordN,
        office.gpsCoordE
        // offices.myFiftyTwo.gpsCoordN,
        // offices.myFiftyTwo.gpsCoordE
      ];

      const userDistance = this.distance(this.localCoord[0], this.localCoord[1], this.officeCoord[0], this.officeCoord[1]);
      if (userDistance <= 10000000000) { // Originalement à 1000m mais augmentée pour le rendu
        // Alert.alert(`Vous avez été identifié dans les locaux de ${officeName}`);
        ToastAndroid.showWithGravityAndOffset(
          `Vous avez été identifié dans les locaux de ${office.officeName}`,
          ToastAndroid.LONG,
          ToastAndroid.BOTTOM,
          0,
          40
        );
        this.props.updateO(office);
      }
    });
  }


  // Get the number of tickets from service now
  getTicketNbr() {
    const { userInfo } = this.props;
    axios
      .get(
        // "https://dev60938.service-now.com/api/now/v1/table/incident",
        "https://dev59358.service-now.com/api/now/v1/table/incident",
        {
          timeout: config.globalTimeout,
          headers: {
            "Accept": "application/json",
            "Authorization": "Basic YWRtaW46cVBORFAxb0l2dmIw",
            'Content-Type': "application/json"
          },
          params: {
            caller_id: `${userInfo.givenName.toLowerCase()}.${userInfo.familyName.toLowerCase()}`,
          }
        }).then((response) => {
          this.setState({ tickets: response.data.result });
        })
      .catch((error) => {
        console.log(error);
      });
  }

  /**
   * Update the unread mails number
   */
  updateUnreadEmail() {
    const token = this.props.serverAuth;
    axios
      .get(
        `https://www.googleapis.com/gmail/v1/users/${this.props.userInfo.id}/messages?q=is%3Aunread&key=${token._55.idToken}&access_token=${token._55.accessToken}`,
        {
          timeout: config.globalTimeout,
          headers: {
            Accept: "application/json"
          }
        }
      )
      .then(response => {
        const data = response.data;
        for (let i in data) {
          if (i == "resultSizeEstimate") {
            this.setState({ nbrEmail: data[i] });
          }
        }
      })
      .catch(err => {
        Alert.alert("Erreur", "Une erreur s'est produite lors de la récupération de vos emails");
      });
  }

  /**
   * Opens a mail.
   */
  openMail() {
    if (Platform.OS === "ios") {
      Linking.openURL("message:");
    } else if (Platform.OS === "android") {
      Linking.openURL("mailto:");
    }
  }

  openPhoneView = () => { Linking.openURL(`tel:${config.standardPhoneNumber}`); }

  /**
   * Render function
   *
   * @return {ReactNode} The react Jsx view
   */
  render() {
    return (
      <Container style={styles.container}>
       {/*  <LinearGradient colors={['#fff', '#fbe4e1']} style={styles.linearGradient}> */}
          <TouchableOpacity onPress={this.openPhoneView}>
            <ImageBackground
              source={require('./../../assets/images/header.png')}
              style={styles.ImgHeader}
            >
              <Text style={styles.h1}>Contacter votre centre de support</Text>
            </ImageBackground>
          </TouchableOpacity>
          <Text style={styles.h2}>Bonjour</Text>
          <Text style={styles.userName}>{this.props.userInfo.name}</Text>
          <IncidentsList tickets={this.state.tickets} />
        {/* </LinearGradient> */}
        <LoadingPopup isActive={this.state.isLoading} />
      </Container>
    );
  }
}

/**
 * { function_description }
 *
 * @param       {<type>}  state   The state
 * @description { As the first argument passed in to connect, mapStateToProps is
 *              used for selecting the part of the data from the store that the
 *              connected component needs. It’s frequently referred to as just
 *              mapState for short. It is called every time the store state
 *              changes.}
 * @return      {Object}  { It receives the entire store state, and should
 *                        return an object of data this component needs. }
 */
function mapStateToProps(state) {
  return {
    userInfo: state.userInfo,
    userStatus: state.userStatus,
    serverAuth: state.serverAuth,
    officeInfo: state.officeInfo
  };
}

/**
 * { function_description }
 *
 * @param       {Function}  dispatch  The dispatch
 * @description { As the second argument passed in to connect,
 *              mapDispatchToProps is used for dispatching actions to the store.
 *              dispatch is a function of the Redux store. You call store.
 *              dispatch to dispatch an action. This is the only way to trigger
 *              a state change.}
 * @return      {function}  { updateS }
 */
function mapDispatchToProps(dispatch) {
  return {
    updateS: userStatus => {
      dispatch(updateStatus(userStatus));
    },
    updateO: officeInfo => {
      dispatch(updateOfficeInfo(officeInfo));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

'use strict';

var React = require('react-native');
// var {vw, vh, vmin, vmax} = require('react-native-viewport-units');


var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({
  container: {
    backgroundColor: '#F4F5FC',
  },
  center: {
    justifyContent: "center",
    alignItems: "center",
    height: 200,
    shadowColor: "rgba(0, 0, 0, 0.5)",
    shadowOffset: {
      width: 0,
      height: 0.7
    },
    shadowRadius: 1.3,
    shadowOpacity: 1
  },
  bigBlue: {
    padding: 20,
    width: "100%",
    height: 225,
    backgroundColor: "#646464",
    shadowColor: "rgba(0, 0, 0, 0.5)",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: {
      width: 0,
      height: 0.7
    },
    shadowRadius: 1.3,
    shadowOpacity: 1
  },
  color: {
    backgroundColor: "#f7f7f7",
    shadowColor: "rgba(0, 0, 0, 0.5)",
    shadowOffset: {
      width: 0,
      height: 0.7
    },
    shadowRadius: 1.3,
    shadowOpacity: 1
  },
  rectangle: {
    zIndex: 5,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    width: "90%",
    height: 90,
    borderRadius: 2.7,
    backgroundColor: "#f7f7f7",
    shadowColor: "rgba(0, 0, 0, 0.5)",
    shadowOffset: {
      width: 0,
      height: 0.7
    },
    shadowRadius: 1.3,
    shadowOpacity: 1
  },
  texte: {
    fontSize: 25,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff",
    marginTop: 15
  },
  userName: {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: 20
  },

  contentContainer: {
    padding: 10,
    marginTop: 10
  },

  h2: {
    textAlign: 'center',
    marginBottom: 20,
    marginTop: 30
  },
  linearGradient: {
    width: '100%',
    height: '100%'
  },
  h1: {
    textAlign: 'center',
    fontSize: 25,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 40,

  },
  ImgHeader: {
    width: '100%',
    height: 320
  },
});
'use strict';

var React = require('react-native');

var {
  StyleSheet,
} = React;

module.exports = StyleSheet.create({
  CardItem : {
    height: 70,
    borderRadius: 8,
    backgroundColor: '#fff',
    display: 'flex',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginTop: 30,
   // maxWidth: '95%',
    width: 360,
  },
  incidentCount : {
    fontWeight: 'bold',
    fontSize: 20
  },
  iconIncident: {
    width: 42,
    height: 42
  }
})
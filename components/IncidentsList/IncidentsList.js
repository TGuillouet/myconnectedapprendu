import React, { Component } from "react";
import {
  CardItem,
  Text
} from "native-base";
import {
  Image,
  TouchableOpacity,
} from "react-native";
import PropTypes from "prop-types";
import { withNavigation } from "react-navigation";
var styles = require('./style');

class IncidentsList extends Component {
  static propTypes = {
    tickets: PropTypes.array.isRequired
  };

  static defaultProps = {
    tickets: []
  };

  navigateToTicketList = () => {
    this.props.navigation.navigate("TicketList", {
      tickets: this.props.tickets
    });
  };

  render() {
    return (
      <TouchableOpacity onPress={this.navigateToTicketList}>
        <CardItem style={styles.CardItem}>
          <Image
            source={require('../../assets/images/iconIncident.png')}
            style={styles.iconIncident}
          />

          <Text>Incidents en cours</Text>
          <Text style={styles.incidentCount}>{this.props.tickets.length}</Text>
        </CardItem>
      </TouchableOpacity>
    );
  }
}

export default withNavigation(IncidentsList);

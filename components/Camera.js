/* eslint-disable no-console */
import React from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Slider,
    Alert,
    CameraRoll,
    PermissionsAndroid,
    Platform
} from "react-native";
// eslint-disable-next-line import/no-unresolved
import { RNCamera } from "react-native-camera";
import { connect } from "react-redux";
// import RNFetchBlob from "react-native-fetch-blob";
import { CreatePickerTicket, updatePickerTicket } from "../reducer";
/*import { FileSystem, FaceDetector, MediaLibrary, Permissions } from 'expo';*/
import Mailer from "react-native-mail";
import firebase from "./firebaseConfig";

/*const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;
*/


const RNFS = require("react-native-fs");

function uploadImage(uri, mime = "image/jpg", name) {
    return new Promise((resolve, reject) => {
        let imgUri = uri;
        let uploadBlob = null;
        const uploadUri = Platform.OS === "ios" ? imgUri.replace("file://", "") : imgUri;
        const { currentUser } = firebase.auth();
        const imageRef = firebase.storage().ref("/images/" + name);

        fetch(uploadUri)
            .then(function(response){ 
                console.log('toototot ', response);
                return response.blob()
            })
            .then(blob => {
                uploadBlob = blob;
                return imageRef.put(blob, { contentType: mime, name: name });
            })
            .then(() => {
                uploadBlob.close();
                return imageRef.getDownloadURL();
            })
            .then(url => {
                resolve(url);
            })
            .catch(error => {
                reject(error);
            });
    });
}

const flashModeOrder = {
    off: "on",
    on: "auto",
    auto: "torch",
    torch: "off"
};
const wbOrder = {
    auto: "sunny",
    sunny: "cloudy",
    cloudy: "shadow",
    shadow: "fluorescent",
    fluorescent: "incandescent",
    incandescent: "auto"
};

async function getUrl(ref) {
    const url = await ref.getDownloadUrl();
    ref.getDownloadURL()
        .then(data => {
            this.setState({ url: data });
            this.setState({ loading: false });
        })
        .catch(error => {
            this.setState({ url: "/images/logoblue.jpg" });
            this.setState({ loading: false });
        });
    return url;
}

async function Permi() {
    if (Platform.OS !== "ios") {
        try {
            let hasPermission = await PermissionsAndroid.check(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
            );
            if (!hasPermission) {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: i18n.t("write_storage_permission"),
                        message: i18n.t("write_storage_permission_message"),
                        buttonNegative: i18n.t("cancel"),
                        buttonPositive: i18n.t("ok")
                    }
                );
                hasPermission = granted !== PermissionsAndroid.RESULTS.GRANTED;
            }
            if (!hasPermission) {
                handleError(i18n.t("error_accessing_storage"));
                return;
            }
        } catch (error) {
            console.warn(error);
        }

        path = `${RNFS.ExternalStorageDirectoryPath}/project_overview_${Number(new Date())}.pdf`;

        try {
            await RNFS.copyFile(pdfFilePath, path);
        } catch (error) {
            handleError(get(error, "message", error));
            return;
        }

        function handleError(error) {
            if (error === "not_available") error = i18n.t("mail_not_available");
            Alert.alert(i18n.t("error"), error, [{ text: i18n.t("ok") }], { cancelable: true });
        }
    }
}

class Camera extends React.Component {
    constructor(props) {
        super(props);
        String.prototype.format = function() {
            a = this;
            for (k in arguments) {
                a = a.replace("{" + k + "}", arguments[k]);
            }
            return a;
        };
    }
    state = {
        flash: "off",
        zoom: 0,
        autoFocus: "on",
        depth: 0,
        type: "back",
        whiteBalance: "auto",
        ratio: "16:9",
        recordOptions: {
            mute: false,
            maxDuration: 5
            // quality: RNCamera.Constants.VideoQuality['480p'],
        },
        isRecording: false,
        url: ""
    };

    alertMessage() {
        // Works on both iOS and Android
        Alert.alert(
            "Alert Title",
            "Alert Title",
            // <Image source={{ uri: this.takePicture().data.uri }} style={{ width: 40, height: 40 }} />,
            [
                {
                    text: "Annuler",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                }
            ],
            { cancelable: false }
        );
    }

    toggleFacing() {
        this.setState({
            type: this.state.type === "back" ? "front" : "back"
        });
    }

    toggleFlash() {
        this.setState({
            flash: flashModeOrder[this.state.flash]
        });
    }

    toggleWB() {
        this.setState({
            whiteBalance: wbOrder[this.state.whiteBalance]
        });
    }

    toggleFocus() {
        this.setState({
            autoFocus: this.state.autoFocus === "on" ? "off" : "on"
        });
    }

    zoomOut() {
        this.setState({
            zoom: this.state.zoom - 0.1 < 0 ? 0 : this.state.zoom - 0.1
        });
    }

    zoomIn() {
        this.setState({
            zoom: this.state.zoom + 0.1 > 1 ? 1 : this.state.zoom + 0.1
        });
    }

    setFocusDepth(depth) {
        this.setState({
            depth
        });
    }

    doRefreshState(datauri) {
        //this.uploadUri(datauri);
        const ticketInfo = { dataUri: datauri };
        const nameUser = this.props.userInfo.name;
        const { navigate } = this.props.navigation;
        const storageRef = firebase.storage().ref("/images/imageOne");
        uploadImage(datauri, "image/jpg", "imageOne")
            .then(url => {
                console.log("efefe  ",url);
                this.setState({ url: url }, () => {
                    this.props.navigation.state.params.onGoBack(url);
                    this.props.navigation.goBack();
                });
            })
            .catch(error => {
                console.warn(error.message);
            });

        const why = this.props.navigation.getParam("why", "Peter");
        const impact = this.props.navigation.getParam("impact", "Pet");
        const description = this.props.navigation.getParam("description", "Pet");
        /*const why = navigate.getParam('why');
      const impact = navigate.getParam('impact');*/
        const bodyString =
            "bonjour,\n" +
            " création d'un ticket depuis l'application APP." +
            "\r\n" +
            "lien de la photo : {0}" +
            "\r\n" +
            "l'object en cause : {1}" +
            "l'impact est : {2}" +
            " description : {3}" +
            "\r\n" +
            "\r\n" +
            "merci de votre prise en compte," +
            "\r\n" +
            "myConnectedApp";

        storageRef.getDownloadURL()/*.then(
            function(url) {
                Mailer.mail(
                    {
                        subject: "Support helpline",
                        recipients: ["inin-script_complexe@helpline.fr"],
                        body: bodyString.format(url, why, impact, description),
                        isHTML: true
                    },
                    (error, event) => {
                        if (error) {
                            handleError(error);
                        }
                    }
                );
            },
            function(error) {
                console.log(error);
            }
        );*/
        console.warn(storageRef);
        
    }

    takePicture = async function() {
        if (this.camera) {
            //Permi();
            const options = { quality: 0.3, base64: true };
            const data = await this.camera.takePictureAsync(options);
            Alert.alert(
                "PHOTO", // tittle
                "valider la photo ?", // message
                [
                    { text: "valider", onPress: () => this.doRefreshState(data.uri) },
                    { text: "recommencer", onPress: () => console.log("OK recommencer") }
                ],
                { cancelable: false }
            );
        }
    };
    takeVideo = async function() {
        if (this.camera) {
            try {
                const promise = await this.camera.recordAsync(this.state.recordOptions);

                if (promise) {
                    this.setState({ isRecording: true });
                    const data = await promise;
                    this.setState({ isRecording: false });
                    //console.warn('takeVideo', data);
                    CameraRoll.saveToCameraRoll(data.uri, "video");
                }
            } catch (e) {
                console.error(e);
            }
        }
    };

    renderCamera() {
        return (
            <RNCamera
                ref={ref => {
                    this.camera = ref;
                }}
                style={{
                    flex: 1
                }}
                type={this.state.type}
                flashMode={this.state.flash}
                autoFocus={this.state.autoFocus}
                zoom={this.state.zoom}
                whiteBalance={this.state.whiteBalance}
                ratio={this.state.ratio}
                focusDepth={this.state.depth}
                permissionDialogTitle={"Permission to use camera"}
                permissionDialogMessage={"We need your permission to use your camera phone"}
                onGoogleVisionBarcodesDetected={({ barcodes }) => {
                    //console.warn(barcodes);
                }}
            >
                <View
                    style={{
                        flex: 0.5,
                        backgroundColor: "transparent",
                        flexDirection: "row",
                        justifyContent: "space-around"
                    }}
                >
                    <TouchableOpacity style={styles.flipButton} onPress={this.toggleFlash.bind(this)}>
                        <Text style={styles.flipText}> FLASH: {this.state.flash} </Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        flex: 0.4,
                        backgroundColor: "transparent",
                        flexDirection: "row",
                        alignSelf: "flex-end"
                    }}
                >
                    <Slider
                        style={{ width: 150, marginTop: 15, alignSelf: "flex-end" }}
                        onValueChange={this.setFocusDepth.bind(this)}
                        step={0.1}
                        disabled={this.state.autoFocus === "on"}
                    />
                </View>
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: "transparent",
                        flexDirection: "row",
                        alignSelf: "flex-end"
                    }}
                >
                    <TouchableOpacity
                        style={[
                            styles.flipButton,
                            {
                                flex: 0.3,
                                alignSelf: "flex-end",
                                backgroundColor: this.state.isRecording ? "white" : "darkred"
                            }
                        ]}
                        onPress={this.state.isRecording ? () => {} : this.takeVideo.bind(this)}
                    >
                        {this.state.isRecording ? (
                            <Text style={styles.flipText}> ☕ </Text>
                        ) : (
                            <Text style={styles.flipText}> REC </Text>
                        )}
                    </TouchableOpacity>
                </View>
                {this.state.zoom !== 0 && (
                    <Text style={[styles.flipText, styles.zoomText]}>Zoom: {this.state.zoom}</Text>
                )}
                <View
                    style={{
                        flex: 0.1,
                        backgroundColor: "transparent",
                        flexDirection: "row",
                        alignSelf: "flex-end"
                    }}
                >
                    <TouchableOpacity
                        style={[styles.flipButton, styles.picButton, { flex: 1, alignSelf: "flex-end" }]}
                        onPress={this.takePicture.bind(this)}
                    >
                        <Text style={styles.flipText}> SNAP </Text>
                    </TouchableOpacity>
                </View>
            </RNCamera>
        );
    }

    render() {
        return <View style={styles.container}>{this.renderCamera()}</View>;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        backgroundColor: "#000"
    },
    flipButton: {
        flex: 0.3,
        height: 40,
        marginHorizontal: 2,
        marginBottom: 10,
        marginTop: 20,
        borderRadius: 8,
        borderColor: "white",
        borderWidth: 1,
        padding: 5,
        alignItems: "center",
        justifyContent: "center"
    },
    flipText: {
        color: "white",
        fontSize: 15
    },
    zoomText: {
        position: "absolute",
        bottom: 70,
        zIndex: 2,
        left: 2
    },
    picButton: {
        backgroundColor: "darkseagreen"
    }
});

/**
 * { function_description }
 *
 * @param       {<type>}  state   The state
 * @description { As the first argument passed in to connect, mapStateToProps is
 *              used for selecting the part of the data from the store that the
 *              connected component needs. It’s frequently referred to as just
 *              mapState for short. It is called every time the store state
 *              changes.}
 * @return      {Object}  { It receives the entire store state, and should
 *                        return an object of data this component needs. }
 */
function mapStateToProps(state) {
    return {
        ticketInfo: state.ticketInfo,
        userInfo: state.userInfo
    };
}

/**
 * { function_description }
 *
 * @param       {Function}  dispatch  The dispatch
 * @description { As the second argument passed in to connect,
 *              mapDispatchToProps is used for dispatching actions to the store.
 *              dispatch is a function of the Redux store. You call store.
 *              dispatch to dispatch an action. This is the only way to trigger
 *              a state change.}
 * @return      {function}  { UpdatePickerT }
 */
function mapDispatchToProps(dispatch) {
    return {
        UpdatePickerT: ticketInfo => {
            dispatch(updatePickerTicket(ticketInfo));
        }
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Camera);

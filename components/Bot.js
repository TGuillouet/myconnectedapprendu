import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Button as Button2,
  TouchableOpacity
} from "react-native";
import {Icon, Button} from "native-base";
import {GiftedChat} from "react-native-gifted-chat";
import {Dialogflow_V2} from "react-native-dialogflow";
import Voice from "react-native-voice";
import {GoogleSignin} from "react-native-google-signin";
import firebase from "./firebaseConfig";
import {dialogflowConfig} from "./env";
import LoadingPopup from "./LoadingPopup";

/*console.log("storageRef444444444 : ", this.state.urlAvatar);*/
const styles = StyleSheet.create({
  transcript: {
    justifyContent:"center",
    alignItems: "center",
    backgroundColor: "#FD6935",
    height: 40,
    width: 300,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: "auto",
    marginRight: "auto",

  },
});

const BOT_USER = {
  _id: 2,
  name: "SmartBot",
  avatar: "this.state.urlAvatar",
};

/**
 * Class for bot.
 *
 * @class      Bot (name)
 */
class Bot extends React.Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: "Bot",
      headerLeft: (
        <Button
          transparent
          onPress={() => this.deconnection(navigation)}
          title="">
          <Icon name="power" />
        </Button>
      ),
    };
  };

  /**
   * { function_description }
   * @description { disconnection button }
   *
   * @param      {<type>}  navigation  The navigation
   */
  static deconnection(navigation) {
    GoogleSignin.signOut();
    navigation.navigate("Login");
  }

  /**
   * Constructs the object.
   *
   * @param      {<type>}  props   The properties
   */
  constructor(props) {
    super(props);

    let firstMsg = {
      _id: 1,
      text:
        "Bonjour, \nje suis Mathilde votre Assistante Virtuel\nComment puis-je vous servir ?",
      createdAt: new Date(),
      user: BOT_USER,
    };

    this.state = {
      messages: [firstMsg],
      recognized: "",
      started: "",
      results: [],
      urlAvatar: "",
    };
    Voice.onSpeechStart = this.onSpeechStart.bind(this);
    Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
  }

  componentDidMount() {
    /*const {currentUser} = firebase.auth();
    const ref = firebase.storage().ref("/images/imageOne");
    ref.getDownloadURL().then(
      function(url) {
        this.setState({
            urlAvatar: url
        });
        console.log("urlAvatar44444444444444 : ",this.state.urlAvatar);
      },
      function(error) {
        console.log(error);
      },
    );*/
    Voice.destroy().then(Voice.removeAllListeners);
    Dialogflow_V2.setConfiguration(
      dialogflowConfig.client_email,
      dialogflowConfig.private_key,
      Dialogflow_V2.LANG_FRENCH,
      dialogflowConfig.project_id,
    );
  }
  /**
   * { function_description }
   *
   * @param      {<type>}  e       { parameter_description }
   */
  onSpeechStart(e) {
    this.setState({
      started: "√",
    });
  }
  onSpeechRecognized(e) {
    this.setState({
      recognized: "√",
    });
  }
  onSpeechResults(e) {
    this.setState({
      results: e.value,
    });
    Dialogflow_V2.requestQuery(
      this.state.results[0],
      result => this.handleGoogleResponse(result),
      error => console.log(error),
    );
  }
  /**
   * Starts a recognition.
   *
   * @param      {<type>}   e       { parameter_description }
   * @return     {Promise}  { description_of_the_return_value }
   */
  async _startRecognition(e) {
    this.setState({
      recognized: "",
      started: "",
      results: [],
    });
    try {
      await Voice.start("en-FR");
    } catch (e) {
      console.error(e);
    }
  }

  /**
   * Sends a bottom response.
   *
   * @param      {<type>}  text    The text
   */
  sendBotResponse(text) {
    let msg = {
      _id: this.state.messages.length + 1,
      text,
      createdAt: new Date(),
      user: BOT_USER,
    };
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, [msg]),
    }));
  }

  /**
   * { function_description }
   *
   * @param      {<type>}  result  The result
   */
  handleGoogleResponse(result) {
    console.log("toto", result);
    console.log(
      "toto2222",
      JSON.stringify(result.queryResult.fulfillmentMessages),
    );
    let text = result.queryResult.fulfillmentMessages[0].text.text[0];
    this.sendBotResponse(text);
  }

  /**
   * { function_description }
   *
   * @param      {<type>}  messages  The messages
   */
  onSend(messages = []) {
    this.setState(previousState => ({
      messages: GiftedChat.append(previousState.messages, messages),
    }));
    let message = messages[0].text;

    Dialogflow_V2.requestQuery(
      message,
      result => this.handleGoogleResponse(result),
      error => console.log(error),
    );
  }

  /**
   * { function_description }
   *
   * @return     {<type>}  { description_of_the_return_value }
   */
  render() {
    return (
      <View style={{flex: 1}}>
        <GiftedChat
          messages={this.state.messages}
          onSend={messages => this.onSend(messages)}
          user={{
            _id: 1,
          }}
          renderLoading={() => <LoadingPopup isActive={true} />}
        />

        <Text> {this.state.results[0]}</Text>

        <TouchableOpacity
          style={styles.transcript}
          onPress={this._startRecognition.bind(this)}
          title="START">
            <Text style={{color: "#fff"}}>START</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Bot;

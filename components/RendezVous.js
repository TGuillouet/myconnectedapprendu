import React, { Component } from "react";
import axios from "axios";
import { View, Platform, Linking, TouchableOpacity, Alert, StyleSheet } from "react-native";
import { connect } from "react-redux";
import {
    CardItem,
    Text,
    Icon,
} from "native-base";
import config from "./../assets/config";

const styles = StyleSheet.create({
    carteStyle: {
        backgroundColor: "#f7f7f7"
    }
    // center: {
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     flex: 1
    // }
});

/**
 * Class for rendez vous.
 *
 * @class      RendezVous (name)
 */
class RendezVous extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            rdvExisting: false,
            rdv: []
        };
    }

    /**
     * { function_description }
     */
    componentDidMount() {
        this.getRdv();
    }

    /**
     * Gets the rdv.
     */
    getRdv() {
        const token = this.props.serverAuth;
        axios
            .get(
                "https://www.googleapis.com/calendar/v3/calendars/" +
                    this.props.userInfo.email +
                    "/events?maxResults=2&key=" +
                    token._55.idToken +
                    "&access_token=" +
                    token._55.accessToken,
                {
                    timeout: config.globalTimeout,
                    headers: {
                        Accept: "application/json"
                    }
                }
            )
            .then(response => {
                const data = response.data;
                for (let i in data) {
                    if (i == "items") {
                        const items = data[i];
                        for (let event in items) {
                            this.setState({
                                rdv: [
                                    ...this.state.rdv,
                                    {
                                        name: items[event].summary,
                                        startEvent: items[event].start.dateTime
                                    }
                                ],
                                rdvExisting: true
                            });
                        }
                    }
                }
            })
            .catch(err => {
                console.error(err);
            });
    }

    /**
     * Opens a cal.
     */
    openCal() {
        if (Platform.OS === "ios") {
            Linking.openURL("calshow:");
        } else if (Platform.OS === "android") {
            Linking.openURL("content://com.android.calendar/time/");
        }
    }

    generateCard = rdv => {
        return (
            <TouchableOpacity onPress={() => this.openCal()}>
                {/* <View >
                    <View>
                        <Icon active name="ios-calendar" />
                        <Text>{rdv.name}</Text>
                    </View>
                </View> */}
                <CardItem style={styles.carteStyle}>
                    <Icon active name="ios-calendar" />
                    <Text>{rdv.name}</Text>
                </CardItem>
            </TouchableOpacity>
        );
    };

    /**
     * { function_description }
     *
     * @return     {<type>}  { description_of_the_return_value }
     */
    render() {
        const noRdv = (
            <CardItem>
                <Text>Vous n'avez pas de rendez-vous</Text>
            </CardItem>
        );
        return <View>{this.state.rdvExisting ? this.state.rdv.map(this.generateCard) : noRdv}</View>;
    }
}

/**
 * { function_description }
 *
 * @param      {<type>}  state   The state
 * @return     {Object}  { description_of_the_return_value }
 */
function mapStateToProps(state) {
    return {
        userInfo: state.userInfo,
        serverAuth: state.serverAuth
    };
}

export default connect(mapStateToProps)(RendezVous);

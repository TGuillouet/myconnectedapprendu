import React from "react";
import axios from "axios";
import { Alert, View, Text, StyleSheet, ScrollView } from "react-native";
import { GoogleSignin } from "react-native-google-signin";
import {
  Form,
  Item,
  Picker,
  Icon,
  Textarea,
  Button,
  Body,
  Container
} from "native-base";
import { connect } from "react-redux";
import { updatePickerTicket } from "../reducer";
import config from "./../assets/config"
import LoadingPopup from "./LoadingPopup";

const styles = StyleSheet.create({
  h1: {fontWeight: "bold", textAlign: "center", fontSize: 30, padding: 20},
  center: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  bigBlue: {
    padding: 20,
    width: "100%",
    height: 225,
    backgroundColor: "#646464",
    shadowColor: "rgba(0, 0, 0, 0.5)",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: {
      width: 0,
      height: 0.7
    },
    shadowRadius: 1.3,
    shadowOpacity: 1
  },
  texte: {
    fontSize: 25,
    fontWeight: "bold",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "center",
    color: "#ffffff",
    marginTop: 15
  },
  picker: { width: "100%"}
});

/**
 * Class for ticket.
 *
 * @class      Ticket (name)
 */
export class Ticket extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Ticket à la volée",
      headerLeft: (
        <Button transparent onPress={() => this.deconnection(navigation)}>
          <Icon name="power" />
        </Button>
      )
    };
  };

  /**
   * { function_description }
   * @description { disconnection button }
   *
   * @param      {<type>}  navigation  The navigation
   */
  static deconnection(navigation) {
    GoogleSignin.signOut();
    navigation.navigate("Login");
  }

  /**
   * Constructs the object.
   *
   * @param      {<type>}  props   The properties
   */
  constructor(props) {
    super(props);
  }

  state = {
    ticketInfo: { selected2: null, description: null, impact: null },
    dataUri: null,
    error: null,
    text: "Description",
    urgency: 1,
    impact: 1,
    probleme: "Autre",
    isOther: false,
    selectedItem: "Imprimante",
    selectedProblem: [],
    officeInfo: {},
  };

  /**
   * [compenentDidMount description]
   *
   * @description { when calling the dial up, it initializes }
   */
  componentDidMount() {
    let { items } = this.props.officeInfo;
    const firstItem = Object.keys(items)[0];
    const firstProblemName = Object.keys(items[firstItem])[0];
    const firstProblem = items[firstItem][firstProblemName];
    this.setState({
      officeInfo: items,
      selectedItem: firstItem,
      probleme: items[firstItem][firstProblemName].name,
      selectedProblem: [firstProblem.short_description, firstProblem.description, firstProblem.impact, firstProblem.urgency]
    }, () => {
      if (this.state.probleme == "Autre") this.setState({ isOther: true });
    })
    /*if (this.state.ticketInfo.dataUri != null) {
      this.props.UpdatePickerT(ticketInfo);*/
  }

  /**
   * { function_description }
   *
   * @param      {<type>}  value:string  The value string
   */
  onValueChangeImpact(value) {
    const ticketInfo = { impact: value };
    this.props.UpdatePickerT(ticketInfo);
    //this.setState({ ticketInfo, error: null });
    this.setState({ impact: value });
  }

  /**
   * { function_description }
   *
   * @param      {<type>}  value:string  The value string
   */
  onValueChangeUrgency(value) {
    const ticketInfo = { selected2: value };
    this.props.UpdatePickerT(ticketInfo);
    //this.setState({ ticketInfo, error: null });
    this.setState({ selected2: value });
  }

  onOutilValueChange(value) {
    this.setState({ selectedItem: value });
  }

  onProblemeValueChange(value) {
    let item = this.state.officeInfo[this.state.selectedItem][value];
    if (value == "Autre")
      this.setState({ isOther: true });
    else
      this.setState({ isOther: false });
    let tab = [item.short_description, item.description, item.impact, item.urgency];
    this.setState({ probleme: value, selectedProblem: tab }, () => { console.log(this.state.selectedProblem) });
  }

  /**
   * { function_description }
   *
   * @param      {<type>}  navigate  The navigate
   */
  info(navigate) {
    /*const ticketInfo = {dataUri: 'toto'};
    this.props.updatePickerTicket(ticketInfo); */
    Alert.alert("result : " + this.state.ticketInfo); //ticketInfo.dataUri);
  }

  contextualMenu = item => {
    return (
      <Picker.Item key={item} id={item} label={item} value={item} />
    );
  }

  generateSubMenu = item => {
    return (
      <Picker.Item key={item.name} id={item.name} label={item.name} value={item.name} />
    );
  }

  onGoBack = (uri) => {
    this.sendTicket(uri);
  }

  ticketSentFailure = () => {
    Alert.alert("Une erreur est survenue, le ticket n'a pas été envoyé.");
  }

  ticketSentSuccess = () => {
    this.setState({ text: "" });
    Alert.alert("Le ticket a été envoyé avec succès.");
  }

  sendTicket = (uri = "") => {
    this.setState({ isLoading: true }, () => {
      const name = this.props.userInfo.givenName.toLowerCase() + "." + this.props.userInfo.familyName.toLowerCase();
      if (this.state.isOther != true) {
        const sDesc = this.state.selectedProblem[0];
        const desc = this.state.selectedProblem[1] + " " + uri;
        const urg = parseInt(this.state.selectedProblem[2]);
        const impt = parseInt(this.state.selectedProblem[3])
        this.setState({ isLoading: false }, () => {
          axios
            .post(
              // "https://dev60938.service-now.com/api/now/v1/table/incident", {
              "https://dev59358.service-now.com/api/now/v1/table/incident", {
              short_description: sDesc,
              description: desc,
              assigment_group: '',
              caller_id: name,
              urgency: urg,
              impact: impt
            },
              {
                timeout: config.globalTimeout,
                headers: {
                  "Accept": "application/json",
                  "Authorization": "Basic YWRtaW46cVBORFAxb0l2dmIw",
                  "Content-Type": "application/json"
                },
              }).then((response) => {
                console.warn(response.config.data);
                this.ticketSentSuccess();
              })
            .catch(function (error) {
              console.log(error);
            });
        });
      } else {
        console.log("HELLO: ", this.state.selectedProblem)
        this.setState({ isLoading: false }, () => {
          const name = this.props.userInfo.givenName.toLowerCase() + "." + this.props.userInfo.familyName.toLowerCase();
          const sDesc = this.state.selectedProblem[0];
          console.log("HOLA SENIOR: ", sDesc);
          axios
            .post(
              // "https://dev60938.service-now.com/api/now/v1/table/incident", {
              "https://dev59358.service-now.com/api/now/v1/table/incident", {
              short_description: sDesc,
              description: this.state.text + " " + uri,
              assigment_group: '',
              caller_id: name,
              urgency: this.state.urgency,
              impact: this.state.impact
            },
              {
                timeout: config.globalTimeout,
                headers: {
                  "Accept": "application/json",
                  "Authorization": "Basic YWRtaW46cVBORFAxb0l2dmIw",
                  "Content-Type": "application/json"
                },
              }).then((response) => {
                console.warn(response.config.data);
                this.ticketSentSuccess();
              })
            .catch(function (error) {
              console.log(error);
            });
        });
      }
    })
  }

  /**
   * { function_description }
   *
   * @return     {<type>}  { form to pre-fill the mail }
   */
  render() {
    const { navigate } = this.props.navigation;
    const data = this.props.ticketInfo.dataUri;
    const otherSelected = (
      <View>
        <Item picker style={{  marginLeft: 10, marginRight: 10 , marginBottom: 15, backgroundColor:"#fff" }}>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="arrow-down" />}
            style={styles.picker}

            placeholder="Impact"
            placeholderStyle={{ color: "#bfc6ea" }}
            placeholderIconColor="#000"
            selectedValue={this.state.impact}
            onValueChange={this.onValueChangeImpact.bind(this)}

          >
            <Picker.Item label="Helpline" value="1" />
            <Picker.Item label="Equipe" value="2" />
            <Picker.Item label="Moi" value="3" />
          </Picker>
        </Item>
        <Item picker style={{ marginLeft: 10, marginRight: 10 , marginBottom: 15, backgroundColor:"#fff" }}>
          <Picker
            mode="dropdown"
            iosIcon={<Icon name="arrow-down" />}
            style={styles.picker}
            placeholder="Urgence"
            placeholderStyle={{ color: "#bfc6ea" }}
            placeholderIconColor="#007aff"
            selectedValue={this.state.urgency}
            onValueChange={this.onValueChangeUrgency.bind(this)}
          >
            <Picker.Item label="Critique" value="1" />
            <Picker.Item label="Majeur" value="2" />
            <Picker.Item label="Mineur" value="3" />
          </Picker>
        </Item>
        <Textarea
          style={{
            marginTop: 15,
            marginLeft: 10,
            marginRight: 10,
            backgroundColor: "#fff",
            borderColor: "#fff"
          }}
          rowSpan={5}
          bordered
          placeholder="Détailler votre Ticket"
          onChangeText={text => this.setState({ text })}
        />
      </View>
    );

    return (
      <Container>
        <ScrollView style={{backgroundColor: "#F4F5FC"}}>
          <Body style={styles.center}>
            <Text style={styles.h1}>Vous avez un incident avec :</Text>
          </Body>
          <Form>
            <View>
              <Item picker style={{ marginLeft: 10, marginRight: 10, marginBottom: 15, backgroundColor:"#fff" }}>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={styles.picker}
                  placeholder="Outil"
                  placeholderStyle={{ color: "#121B41" }}
                  placeholderIconColor="#F4F5FC"
                  selectedValue={this.state.selectedItem}
                  onValueChange={this.onOutilValueChange.bind(this)}
                >
                  {Object.keys(this.state.officeInfo).map(this.contextualMenu)}
                </Picker>
              </Item>
            </View>
            <View>
              <Item picker style={{  marginLeft: 10, marginRight: 10 , marginBottom: 15, backgroundColor:"#fff" }}>
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={styles.picker}
                  placeholder="Problème"
                  placeholderStyle={{ color: "#bfc6ea" }}
                  placeholderIconColor="#007aff"
                  selectedValue={this.state.probleme}
                  onValueChange={this.onProblemeValueChange.bind(this)}
                >
                  {Object.values(this.state.officeInfo[this.state.selectedItem] || []).map(this.generateSubMenu)}
                </Picker>
              </Item>
            </View>
            {this.state.isOther == true ? otherSelected : null}
          </Form>
          <Button
            block
            warning
            style={{ marginTop: 20, marginLeft: 20, marginRight: 20, backgroundColor: "transparent", borderColor: "#121B41", borderStyle: "solid", borderWidth: 2, boxShadow: "none" }}
            onPress={() =>
              this.props.navigation.navigate("CameraModule", {
                why: this.state.urgency,
                impact: this.state.impact,
                description: this.state.text,
                onGoBack: this.onGoBack
              })
            }
          >
            <Text style={{ fontWeight: "bold", color: "#121B41"}}>PRENDRE UNE PHOTO</Text>
            <Icon name="ios-camera" style={{color: "#121B41"}}/>
          </Button>
          <Button
            block
            danger
            style={{ marginTop: 20, marginLeft: 20, marginRight: 20, backgroundColor: "#FD6935" }}
            onPress={() => this.sendTicket()}
          >
            <Text style={{ fontWeight: "bold", color: "white" }}>ENVOYER</Text>
          </Button>
        </ScrollView>
        <LoadingPopup isActive={this.state.isLoading} />
      </Container>
    );
  }
}

/**
 * { function_description }
 *
 * @param       {<type>}  state   The state
 * @description { As the first argument passed in to connect, mapStateToProps is
 *              used for selecting the part of the data from the store that the
 *              connected component needs. It’s frequently referred to as just
 *              mapState for short. It is called every time the store state
 *              changes.}
 * @return      {Object}  { It receives the entire store state, and should
 *                        return an object of data this component needs. }
 */
function mapStateToProps(state) {
  return {
    userInfo: state.userInfo,
    userStatus: state.userStatus,
    serverAuth: state.serverAuth,
    ticketInfo: state.ticketInfo,
    officeInfo: state.officeInfo
  };
}

/**
 * { function_description }
 *
 * @param       {Function}  dispatch  The dispatch
 * @description { As the second argument passed in to connect,
 *              mapDispatchToProps is used for dispatching actions to the store.
 *              dispatch is a function of the Redux store. You call store.
 *              dispatch to dispatch an action. This is the only way to trigger
 *              a state change.}
 * @return      {function}  { UpdatePickerT }
 */
function mapDispatchToProps(dispatch) {
  return {
    UpdatePickerT: ticketInfo => {
      dispatch(updatePickerTicket(ticketInfo));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Ticket);

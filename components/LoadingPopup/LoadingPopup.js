import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from "./styles";

export default class LoadingPopup extends Component {
  render() {
    if (!this.props.isActive) return null;
    return (
      <View style={[styles.background, styles.centered]}>
        <View style={[styles.popup, styles.centered]}>
          <ActivityIndicator size={64} />
        </View>
      </View>
    )
  }
}

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    height: "100%",
    position: "absolute",
    backgroundColor: "rgba(0, 0, 0, 0.5)"
  },
  popup: {
    position: "relative",
    width: 100,
    height: 100,
    borderRadius: 20,
    backgroundColor: "white"
  },
  centered: {
    justifyContent: "center",
    alignItems: "center"
  }
});

export default styles;

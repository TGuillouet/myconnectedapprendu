import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(235, 235, 235, 0.5)',
    flex: 1
  },
  incidentContainer: {
    maxWidth: '95%',
    minWidth: '95%',
    width: '95%',
    height: 70,

    paddingHorizontal: "5%",

    borderRadius: 8,
    marginTop: 20,

    backgroundColor: '#fff',
    flexDirection: 'row',

    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  shadowed: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  linearGradient: {
    width: '100%',
    height: '100%'
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',

    height: 30
  }
});

export default styles;
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import styles from './styles';

export default class TicketsList extends Component {

  static navigationOptions = {
    title: "Vos tickets"
  }

  state = {
    tickets: []
  };

  componentDidMount() {
    this.setState({ tickets: this.props.navigation.getParam('tickets', []) });
  }

  getIncidentStateString(stateStr) {
    const stateInt = parseInt(stateStr);
    switch (stateInt) {
      case 1: return "Nouveau"
      case 2: return "En cours"
      case 3: return "En attente"
      case 4: return "Résolu"
      case 5: return "Fermé"
      case 6:
      default:
        return "Annulé"
    }
  }

  generateTicketView = ({ number, incident_state }) => {
    const stringIncidentState = this.getIncidentStateString(incident_state);
    return (
      <View key={number} style={[styles.incidentContainer, styles.shadowed]}>
        <Text>{number}</Text>
        <Text>{stringIncidentState}</Text>
      </View>
    );
  };

  render() {
    return (
      <>
        <View style={styles.header}>
          <Text>Référence</Text>
          <Text>Status</Text>
        </View>
        <ScrollView contentContainerStyle={styles.container}>
          {this.state.tickets.map(this.generateTicketView)}
        </ScrollView>
      </>
    )
  }
}

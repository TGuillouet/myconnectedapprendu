import React, { Component } from 'react';
import { View, PermissionsAndroid } from 'react-native';
import { WebView } from "react-native-webview";

class DamovoScreen extends Component {

  baseUrl = "http://webrtc.damovo.com.pl/hl/";

  state = {
    displayed: true,
    canGoBack: false,
    url: "http://webrtc.damovo.com.pl/hl/"
  };

  componentDidMount() {
    this.props.navigation.addListener('willFocus', (route) => {
      if (route.state.routeName === "Damovo") this.setState({ displayed: true })
    });

    this.props.navigation.addListener('willBlur', (route) => {
      if (route.state.routeName === "Damovo") this.setState({ displayed: false })
    });

    PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.CAMERA,
      PermissionsAndroid.PERMISSIONS.RECORD_AUDIO
    ]).then((result) => {
      console.warn(result);
    }).catch((err) => {
      console.error(err);
    });
  }

  onNavigationStateChange = (navState) => {
    console.log(navState);
    this.setState({
      canGoBack: navState.canGoBack,
      url: navState.url
    });
  };

  render() {
    return (
      (this.state.displayed) ?
        <WebView
          javaScriptEnabled={true}
          domStorageEnabled={true}
          onNavigationStateChange={this.onNavigationStateChange}
          allowsInlineMediaPlayback
          source={{ uri: this.state.url }} /> : null
    )
  }
}


export default DamovoScreen;

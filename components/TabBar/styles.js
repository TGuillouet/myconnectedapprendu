import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 0.1
  },
  subContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 20,
    paddingRight: 20,
  },
  button: { width: 30, height: 30},
  icon: { maxWidth: "100%", maxHeight: '100%', marginBottom: 5},
  icon_text:{textAlign: 'center', color: '#8090D4', fontSize: 10}
});

export default styles;
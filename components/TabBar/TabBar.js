import React from "react";
import { View, TouchableOpacity, Image, Alert, Linking, Text } from "react-native";
import { Icon } from "native-base";
import axios from "axios";
import { connect } from "react-redux";

import styles from './styles'
import config from './../../assets/config.json';

const images_dir = './../../assets/images';
const url = "http://webrtc.damovo.com.pl/hl/";

const openWall = () => {
  axios.get(config.wallUnlockUrl)
    .then(r => Alert.alert("Le wall est ouvert"))
    .catch(e => Alert.alert("Erreur", "Une erreur est survenue lors de l'ouverture du wall, veuillez vérifier votre connexion internet"));
};

const openBrowser = () => {
  Linking.canOpenURL(url)
    .then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Alert.alert("Erreur", "Cet url n'est pas supporté");
      }
    })
    .catch(err => console.error(err))
};

const navigate = (route, nav) => {
  return () => nav.navigate(route);
}

const tabBarComponent = ({ officeInfo, navigation }) => (
  <View style={styles.container}>
    <View style={styles.subContainer}>
      <TouchableOpacity onPress={navigate("HomeScreen", navigation)} style={styles.button}>
        <Image source={require(`${images_dir}/iconHome.png`)} style={styles.icon} />
        <Text style={styles.icon_text}>Accueil</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={openWall} style={styles.button}>
      <Image source={require(`${images_dir}/iconWall.png`)} style={styles.icon} />
      <Text style={styles.icon_text}>Wall</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigate("Ticket", navigation)} style={styles.button} disabled={(Object.keys(officeInfo).length === 0)} >
        <Image source={require(`${images_dir}/iconTicket.png`)} style={styles.icon} />
        <Text style={styles.icon_text}>Ticket</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={navigate("Bot", navigation)} style={styles.button} >
        <Image source={require(`${images_dir}/iconChat.png`)} style={styles.icon} />
        <Text style={styles.icon_text}>Chat</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={openBrowser/* navigate("Damovo", navigation) */} style={styles.button} >
      {/* <Image source={require(`${images_dir}/iconChat.png`)} style={styles.icon} />*/}
        <Icon type="MaterialCommunityIcons" name="message-video" active style={{ fontSize: 30, color: "#8090d4" }} />
        <Text style={styles.icon_text}>Vidéo</Text>
       </TouchableOpacity> 
    </View>
  </View>
);

const mapStateToProps = ({ officeInfo }) => ({ officeInfo });

export default connect(mapStateToProps, null)(tabBarComponent);
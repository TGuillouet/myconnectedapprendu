import firebase from "firebase";

var config = {
	apiKey: "AIzaSyCNa6nvGxEQwXbUK5mgDPIXaM0VGZKEhgc",
	authDomain: "veolia-c8931.firebaseapp.com",
	databaseURL: "https://veolia-c8931.firebaseio.com",
	projectId: "veolia-c8931",
	storageBucket: "veolia-c8931.appspot.com",
	messagingSenderId: "747855608949"
};

export default firebase.initializeApp(config);

import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  Button,
  ImageBackground,
  relative,
  absolute,
  Image
} from "react-native";
import { GoogleSignin, GoogleSigninButton, statusCodes } from "react-native-google-signin";
import { connect } from "react-redux";
import firebase from "./firebaseConfig";
import { updateUser, updateAuth } from "../reducer";

/**
 * Class for login.
 *
 * @class      Login (name)
 */
class Login extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    userInfo: null,
    isLoginScreenPresented: false,
    error: null
  };
  
  /**
   * { function_description }
   *
   * @return     {button}  { return the button for connect with a account google
   *                       }
   */
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{ width: "100%", height: "100%", position: relative, backgroundColor: "#FD6935" }}
      >
        <Image
          source={require('./../assets/images/mcc_app_B.png')}
          style={{ width: "80%", resizeMode: "contain", alignSelf: "center" }}
          />
        <GoogleSigninButton
          style={{
            width: "80%",
            height: 50,
            position: 'absolute',
            top: "85%",
            left: "10%"
          }}
          size={GoogleSigninButton.Size.Standard}
          color={GoogleSigninButton.Color.Auto}
          onPress={() => this._signIn(navigate)}
        />
     
      </View>
    );
  }

  /**
   * { function_description }
   *
   * @param      {Function}  navigate  The navigate description { go in the home
   *                                   if the account is registered in the
   *                                   database }
   */
  googleConnection(navigate) {
    this._isSignedIn();
    if (this.state.isLoginScreenPresented != false) {
      navigate("HomeScreen");
    } else {
      this._signOut();
    }
  }

  /**
   * { function_description }
   *
   * @param      {Function}  navigate  The navigate description { regain the
   *                                   status of the user }
   */
  userAuth(navigate) {
    var database = firebase.database();
    const userInfo = this.state.userInfo;
    this.props.updateU(userInfo.user);
    this.props.updateA(GoogleSignin.getTokens());
    database
      .ref("/Users/")
      .orderByChild("email")
      .equalTo(userInfo.user.email)
      .once("value", function(snapshot) {
        if (snapshot.val() == null) {
          // L'utilisateur n'a pas été trouvé
          this._signOut;
          Alert.alert(
            "Vous n'avez actuellement pas accès à cette application, veuillez-vous rapprochez du support pour vous rajoutez."
          );
        } else {
          snapshot.forEach(function(childSnapshot) {
            navigate("HomeScreen", { statut: childSnapshot.val().statut });
            // Suite du programme
          });
        }
      });
  }

  _signIn = async navigate => {
    try {
      GoogleSignin.configure({
        scopes: [
          "https://www.googleapis.com/auth/calendar.events.readonly",
          "https://www.googleapis.com/auth/gmail.readonly"
        ],
        webClientId: "747855608949-l00k6gj76cq9piorgnub701hnfno3282.apps.googleusercontent.com"
      });
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({ userInfo, error: null });
      this.userAuth(navigate);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        Alert.alert("Connexion annulée");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        Alert.alert("Connexion en cours");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert("Play services non disponible ou non à jour");
      } else {
        Alert.alert("Something went wrong", error.toString());
        this.setState({
          error
        });
      }
    }
  };

  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    this.setState({ isLoginScreenPresented: !isSignedIn });
  };

  _signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();

      this.setState({ userInfo: null, error: null });
    } catch (error) {
      this.setState({
        error
      });
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

/**
 * { function_description }
 *
 * @param       {<type>}  state   The state
 * @description { As the first argument passed in to connect, mapStateToProps is
 *              used for selecting the part of the data from the store that the
 *              connected component needs. It’s frequently referred to as just
 *              mapState for short. It is called every time the store state
 *              changes.}
 * @return      {Object}  { It receives the entire store state, and should
 *                        return an object of data this component needs. }
 */
function mapStateToProps(state) {
  return {
    userInfo: state.userInfo
  };
}

/**
 * { function_description }
 *
 * @param       {Function}  dispatch  The dispatch
 * @description { As the second argument passed in to connect,
 *              mapDispatchToProps is used for dispatching actions to the store.
 *              dispatch is a function of the Redux store. You call store.
 *              dispatch to dispatch an action. This is the only way to trigger
 *              a state change.}
 * @return      {function}  { updateU and updateA }
 */
function mapDispatchToProps(dispatch) {
  return {
    updateU: userInfo => {
      dispatch(updateUser(userInfo));
    },
    updateA: serverAuth => {
      dispatch(updateAuth(serverAuth));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    quotes: ["error", "doubles"],
    "no-duplicates": ["error"]
  }
};
